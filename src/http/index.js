import axios from 'axios'
import {Message} from 'element-ui'
axios.defaults.baseURL='http://localhost:8888/api/private/v1/'
export default 
function http(url,data,method,params){
    return new Promise((resolve,reject)=>{
        axios({
            url,
            data,
            method,
            params
        })
        .then(res=>{
            if(res.data.meta.status>=200&&res.data.meta.status<300){
                resolve(res.data)
            }else{
                Message.error(res.data.meta.msg)
                reject(res.data.meta)
            }
        })
        .catch(err=>{
            Message.error(err)
            reject(err)
        })
    })
}